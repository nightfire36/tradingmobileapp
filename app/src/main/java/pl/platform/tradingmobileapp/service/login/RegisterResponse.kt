package pl.platform.tradingmobileapp.service.login

data class RegisterResponse (
    val status: Int,
    val invalidFields: Int
)

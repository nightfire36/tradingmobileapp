package pl.platform.tradingmobileapp.service.http.response

class ModelStatusCode {
    companion object {

        // default success code
        const val STATUS_SUCCESS = 10

        // error codes

        // take position error codes
        const val ERROR_NOT_ENOUGH_MONEY = 101

        // place order error codes
        const val ERROR_DB_SAVE_EXCEPTION = 201

        // close position error codes
        const val ERROR_DB_DELETE_EXCEPTION = 301
        const val ERROR_BELONGS_TO_ANOTHER_USER = 302
        const val ERROR_CANNOT_FIND_POSITION = 303

        // cancel order error codes
        const val ERROR_CANNOT_FIND_ORDER = 401

        // registration error codes
        const val ERROR_EMAIL_ALREADY_EXISTS = 501
        const val ERROR_INVALID_USER_DATA = 502
        const val ERROR_INVALID_FIELD_VALUE = 503

        // login error codes
        const val ERROR_BAD_CREDENTIALS = 601
        const val ERROR_UNKNOWN_LOGIN_ERROR = 602
    }
}

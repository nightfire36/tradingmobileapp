package pl.platform.tradingmobileapp.service.login

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import pl.platform.tradingmobileapp.activity.login.RegisterCallback
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.JsonRequestService
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl
import pl.platform.tradingmobileapp.service.http.dto.UserDto
import pl.platform.tradingmobileapp.service.http.response.ModelStatusCode

class RegisterService(
    private val firstName: String, private val lastName: String,
    private val email: String, private val password: String,
    private val registerCallback: RegisterCallback) : JsonRequestAsyncCallback {

    fun register(): Boolean {
        val registerDto =
            UserDto(
                firstName,
                lastName,
                email,
                password
            )

        JsonRequestService(this).execute(ApiEndpointUrl.REGISTER,
            ObjectMapper().writeValueAsString(registerDto))
        return true
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        Log.d("2", response.responseBody)
        val registerResponse: RegisterResponse =
            ObjectMapper()
                .registerModule(KotlinModule())
                .readValue(response.responseBody)
        if(registerResponse.status == ModelStatusCode.STATUS_SUCCESS) {
            registerCallback.onRegisterSuccess()
        } else {
            registerCallback.onRegisterFail(registerResponse.invalidFields)
        }
    }
}

package pl.platform.tradingmobileapp.service

interface JsonRequestAsyncCallback {
    fun onTaskCompleted(response: JsonRequestResponse)
}

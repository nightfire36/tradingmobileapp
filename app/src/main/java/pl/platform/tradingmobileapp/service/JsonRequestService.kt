package pl.platform.tradingmobileapp.service

import android.os.AsyncTask
import android.util.Log
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class JsonRequestService(
    private val callback: JsonRequestAsyncCallback,
    private val requestMethod: String = "POST"
) : AsyncTask<String, Int, JsonRequestResponse>() {

    override fun doInBackground(vararg params: String?): JsonRequestResponse {
        if(params.size >= 1) {
            params[0]?.let { url ->
                connect(url)?.let { connection ->
                    try {
                        connection.requestMethod = requestMethod

                        connection.setRequestProperty("Content-Type", "application/json")

                        connection.setRequestProperty("User-Agent",
                            "Mozilla/5.0 (Android 9; Mobile; rv:68.0) Gecko/68.0 Firefox/68.0")
                        connection.setRequestProperty("Accept-Language", "pl-PL, pl;q=0.9, en;q=0.8")

                        if(params.size >= 2) {
                            params[1]?.let { requestBody ->
                                connection.doOutput = true

                                connection.setRequestProperty("Content-Length", requestBody.length.toString())

                                val outputStream = DataOutputStream(connection.outputStream)

                                outputStream.writeBytes(requestBody)
                                outputStream.flush()

                                outputStream.close()
                            }
                        } else {
                            connection.doOutput = false
                            connection.setRequestProperty("Content-Length", "0")
                        }

                        Log.d("6", connection.requestProperties.toString())

                        if(connection.responseCode == 200) {
                            Log.d("6", connection.headerFields.toString())
                            return JsonRequestResponse(connection.responseCode, httpRequest(connection))
                        } else {
                            val res = JsonRequestResponse(connection.responseCode, "")
                            Log.d("6", connection.headerFields.toString())
                            return res
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }
        return JsonRequestResponse(-1, "")
    }

    override fun onPostExecute(response: JsonRequestResponse) {
        callback.onTaskCompleted(response)
    }

    private fun connect(url: String): HttpURLConnection? {
        return try {
            URL(url).openConnection() as HttpURLConnection
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }

    private fun httpRequest(connection: HttpURLConnection): String {
        return try {
            connection.inputStream.bufferedReader().use(BufferedReader::readLine)
        } catch (e: IOException) {
            e.printStackTrace()
            ""
        }
    }
}

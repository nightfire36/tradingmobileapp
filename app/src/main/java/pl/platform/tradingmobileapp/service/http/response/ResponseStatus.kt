package pl.platform.tradingmobileapp.service.http.response

data class ResponseStatus(
    val status: Int
)

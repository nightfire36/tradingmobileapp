package pl.platform.tradingmobileapp.service.pending

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import pl.platform.tradingmobileapp.activity.platform.tab.pending.PendingCallback
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.JsonRequestService
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl

class PendingService(private val  pendingCallback: PendingCallback) : JsonRequestAsyncCallback {

    fun getPendingOrders() {
        JsonRequestService(this).execute(ApiEndpointUrl.PENDING_ORDERS);
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        Log.d("2", "pending orders task completed")
        Log.d("2", response.responseBody)

        if(response != null && response.responseBody.length > 0) {
            val receivedPendingOrders: List<PendingOrderRecord> =
                ObjectMapper()
                    .registerModule(KotlinModule())
                    .readValue(response.responseBody)
            pendingCallback.onGetPendingOrdersSuccess(receivedPendingOrders)
        } else {
            pendingCallback.onGetPendingOrdersFail(0)
        }
    }
}
package pl.platform.tradingmobileapp.service.http.dto

class LoginDto(
    val login: String,
    val password: String
)

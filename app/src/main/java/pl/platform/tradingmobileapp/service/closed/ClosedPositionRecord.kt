package pl.platform.tradingmobileapp.service.closed

import java.io.Serializable
import java.math.BigDecimal
import java.util.*

data class ClosedPositionRecord(
    val tid: Long,
    val uid: Long,
    val currencyPair: String,
    val amount: BigDecimal,
    val openingPrice: BigDecimal,
    val closingPrice: BigDecimal,
    val openingTimestamp: Date,
    val closingTimestamp: Date,
    val profit: BigDecimal,
    val longPosition: Boolean
) : Serializable
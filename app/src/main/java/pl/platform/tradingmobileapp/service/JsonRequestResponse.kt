package pl.platform.tradingmobileapp.service

data class JsonRequestResponse(
    val status: Int,
    val responseBody: String
)

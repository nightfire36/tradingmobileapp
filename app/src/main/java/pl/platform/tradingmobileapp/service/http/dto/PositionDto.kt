package pl.platform.tradingmobileapp.service.http.dto

import java.math.BigDecimal

data class PositionDto(
    val pair: String,
    val amount: BigDecimal
)

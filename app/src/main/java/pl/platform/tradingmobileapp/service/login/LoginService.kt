package pl.platform.tradingmobileapp.service.login

import android.util.Log
import pl.platform.tradingmobileapp.activity.login.LoginCallback
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestService

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl
import pl.platform.tradingmobileapp.service.http.dto.LoginDto
import pl.platform.tradingmobileapp.service.http.response.ModelStatusCode
import pl.platform.tradingmobileapp.service.http.response.ResponseStatus

class LoginService(private val email: String, private val password: String,
                   private val loginCallback: LoginCallback) : JsonRequestAsyncCallback {

    fun login(): Boolean {

        val loginDto =
            LoginDto(
                email,
                password
            )

        JsonRequestService(this).execute(ApiEndpointUrl.LOGIN,
            ObjectMapper().writeValueAsString(loginDto))
        return true
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        Log.d("1", response.responseBody)

        val loginResponse: ResponseStatus =
            ObjectMapper()
                .registerModule(KotlinModule())
                .readValue(response.responseBody)

        if(loginResponse.status == ModelStatusCode.STATUS_SUCCESS) {
            loginCallback.onLoginSuccess()
        }
        else {
            loginCallback.onLoginFail(loginResponse.status)
        }
    }
}

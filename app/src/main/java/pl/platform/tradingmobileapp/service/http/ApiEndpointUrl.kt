package pl.platform.tradingmobileapp.service.http

class ApiEndpointUrl {
    companion object {
        const val LOGIN_CHECK = "http://10.0.2.2:5000/mobile/login_check"
        const val LOGIN = "http://10.0.2.2:5000/mobile/login"
        const val REGISTER = "http://10.0.2.2:5000/mobile/register"
        const val ACCOUNT_INFO = "http://10.0.2.2:5000/mobile/account_info"
        const val RATES = "http://10.0.2.2:5000/mobile/rates"
        const val CLOSED_POSITIONS = "http://10.0.2.2:5000/mobile/closed_positions"
        const val OPENED_POSITIONS = "http://10.0.2.2:5000/mobile/opened_positions"
        const val PENDING_ORDERS = "http://10.0.2.2:5000/mobile/pending_orders"
        const val LONG_POSITION = "http://10.0.2.2:5000/mobile/long_position"
        const val SHORT_POSITION = "http://10.0.2.2:5000/mobile/short_position"
        const val ORDER_LONG = "http://10.0.2.2:5000/mobile/order_long"
        const val ORDER_SHORT = "http://10.0.2.2:5000/mobile/order_short"
        const val CLOSE = "http://10.0.2.2:5000/mobile/close"
        const val ORDER_CLOSURE = "http://10.0.2.2:5000/mobile/order_closure"
        const val CANCEL_ORDER = "http://10.0.2.2:5000/mobile/cancel_order"

    }
}
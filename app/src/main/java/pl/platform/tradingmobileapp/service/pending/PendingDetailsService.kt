package pl.platform.tradingmobileapp.service.pending

import com.fasterxml.jackson.databind.ObjectMapper
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.JsonRequestService
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl
import pl.platform.tradingmobileapp.service.http.dto.IdDto

class PendingDetailsService : JsonRequestAsyncCallback {

    fun cancelOrderRequest(oid: Long) {
        val idDto = IdDto(oid)
        JsonRequestService(this).execute(ApiEndpointUrl.CANCEL_ORDER,
            ObjectMapper().writeValueAsString(idDto))
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

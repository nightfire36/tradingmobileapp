package pl.platform.tradingmobileapp.service.opened

import com.fasterxml.jackson.databind.ObjectMapper
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.JsonRequestService
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl
import pl.platform.tradingmobileapp.service.http.dto.IdDto
import pl.platform.tradingmobileapp.service.http.dto.OrderClosureDto
import java.math.BigDecimal

class OpenedDetailsService : JsonRequestAsyncCallback {

    fun closePositionRequest(tid: Long) {
        val idDto = IdDto(tid)
        JsonRequestService(this).execute(ApiEndpointUrl.CLOSE,
            ObjectMapper().writeValueAsString(idDto))
    }

    fun orderClosureRequest(tid: Long, trigger: Boolean, price: BigDecimal) {
        val orderClosureDto = OrderClosureDto(tid, trigger, price)
        JsonRequestService(this).execute(ApiEndpointUrl.ORDER_CLOSURE,
            ObjectMapper().writeValueAsString(orderClosureDto))
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

package pl.platform.tradingmobileapp.service.newposition

import com.fasterxml.jackson.databind.ObjectMapper
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.JsonRequestService
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl
import pl.platform.tradingmobileapp.service.http.dto.OrderDto
import pl.platform.tradingmobileapp.service.http.dto.PositionDto
import java.math.BigDecimal

class NewPositionDetailsService : JsonRequestAsyncCallback {

    fun buyRequest(pair: String, amount: BigDecimal) {
        val positionDto = PositionDto(pair, amount)
        JsonRequestService(this).execute(ApiEndpointUrl.LONG_POSITION,
            ObjectMapper().writeValueAsString(positionDto))
    }

    fun sellRequest(pair: String, amount: BigDecimal) {
        val positionDto = PositionDto(pair, amount)
        JsonRequestService(this).execute(ApiEndpointUrl.SHORT_POSITION,
            ObjectMapper().writeValueAsString(positionDto))
    }

    fun orderBuyRequest(pair: String,
                        amount: BigDecimal,
                        trigger: Boolean,
                        price: BigDecimal) {
        val orderDto = OrderDto(pair, amount, trigger, price)
        JsonRequestService(this).execute(ApiEndpointUrl.ORDER_LONG,
            ObjectMapper().writeValueAsString(orderDto))
    }

    fun orderSellRequest(pair: String,
                         amount: BigDecimal,
                         trigger: Boolean,
                         price: BigDecimal) {
        val orderDto = OrderDto(pair, amount, trigger, price)
        JsonRequestService(this).execute(ApiEndpointUrl.ORDER_SHORT,
            ObjectMapper().writeValueAsString(orderDto))
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

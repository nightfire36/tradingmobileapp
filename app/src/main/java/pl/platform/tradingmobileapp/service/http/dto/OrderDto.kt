package pl.platform.tradingmobileapp.service.http.dto

import java.math.BigDecimal

data class OrderDto(
    val pair: String,
    val amount: BigDecimal,
    val trigger: Boolean,
    val price: BigDecimal
)

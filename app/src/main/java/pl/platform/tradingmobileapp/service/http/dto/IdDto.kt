package pl.platform.tradingmobileapp.service.http.dto

data class IdDto(
    val id: Long
)

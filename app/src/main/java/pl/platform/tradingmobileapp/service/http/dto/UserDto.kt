package pl.platform.tradingmobileapp.service.http.dto

class UserDto(
    val firstName: String,
    val lastName: String,
    val email: String,
    val password: String
)

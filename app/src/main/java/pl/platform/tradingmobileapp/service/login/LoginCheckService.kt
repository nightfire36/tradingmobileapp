package pl.platform.tradingmobileapp.service.login

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import pl.platform.tradingmobileapp.activity.LoginCheckCallback
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.JsonRequestService
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl
import pl.platform.tradingmobileapp.service.http.response.ModelStatusCode
import pl.platform.tradingmobileapp.service.http.response.ResponseStatus

class LoginCheckService(private val loginCheckCallback: LoginCheckCallback) : JsonRequestAsyncCallback {

    fun loginCheck() {
        JsonRequestService(this).execute(ApiEndpointUrl.LOGIN_CHECK)
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        Log.d("4", response.responseBody)

        if(response.status == 200) {
            val responseStatus: ResponseStatus =
                ObjectMapper()
                    .registerModule(KotlinModule())
                    .readValue(response.responseBody)
            if(responseStatus.status == ModelStatusCode.STATUS_SUCCESS) {
                return loginCheckCallback.onStatusSuccess()
            }
        }
        loginCheckCallback.onUnauthorized()
    }
}

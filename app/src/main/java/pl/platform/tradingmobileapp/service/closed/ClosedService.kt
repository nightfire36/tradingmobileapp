package pl.platform.tradingmobileapp.service.closed

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import pl.platform.tradingmobileapp.activity.platform.tab.closed.ClosedCallback
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.JsonRequestService
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl

class ClosedService(private val closedCallback: ClosedCallback) : JsonRequestAsyncCallback {

    fun getClosedPositions() {
        JsonRequestService(this).execute(ApiEndpointUrl.CLOSED_POSITIONS)
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        Log.d("2", "closed positions task completed")
        Log.d("2", response.responseBody)

        if(response != null && response.responseBody.length > 0) {
            val receivedClosedPositions: List<ClosedPositionRecord> =
                ObjectMapper()
                    .registerModule(KotlinModule())
                    .readValue(response.responseBody)
            closedCallback.onGetClosedPositionsSuccess(receivedClosedPositions)
        } else {
            closedCallback.onGetClosedPositionsFail(0)
        }
    }
}

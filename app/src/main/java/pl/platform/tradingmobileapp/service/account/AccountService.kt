package pl.platform.tradingmobileapp.service.account

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import pl.platform.tradingmobileapp.activity.platform.tab.account.AccountCallback
import pl.platform.tradingmobileapp.service.JsonRequestAsyncCallback
import pl.platform.tradingmobileapp.service.JsonRequestResponse
import pl.platform.tradingmobileapp.service.JsonRequestService
import pl.platform.tradingmobileapp.service.http.ApiEndpointUrl

class AccountService(private val accountCallback: AccountCallback) : JsonRequestAsyncCallback {

    fun getAccountInfo() {
        JsonRequestService(this).execute(ApiEndpointUrl.ACCOUNT_INFO)
    }

    override fun onTaskCompleted(response: JsonRequestResponse) {
        Log.d("2", "account info task completed")
        Log.d("2", response.responseBody)

        if(response != null && response.responseBody.length > 0) {
            val accountInfoResponse: AccountInfo = ObjectMapper()
                .registerModule(KotlinModule())
                .readValue(response.responseBody)
            accountCallback.onGetAccountInfoSuccess(accountInfoResponse)
        } else {
            accountCallback.onGetAccountInfoFailed(0)
        }
    }
}

package pl.platform.tradingmobileapp.service.http.dto

import java.math.BigDecimal

data class OrderClosureDto(
    val tid: Long,
    val trigger: Boolean,
    val price: BigDecimal
)

package pl.platform.tradingmobileapp.service

import android.content.Context
import android.net.Uri
import android.util.Log
import java.net.CookieHandler
import java.net.CookieManager
import java.net.HttpCookie
import java.net.URI

class InitializeCookieManager(private val context: Context) {

    fun initialize() {
        Log.d("5", "Cookies initialization")
        val cookieManager = CookieManager()
        val sharedPreferences = context.getSharedPreferences("trading_mobile_app", Context.MODE_PRIVATE)
        Log.d("5", "Shared preferences: " + sharedPreferences.all.size)
        sharedPreferences.all.forEach {
            Log.d("5", it.value.toString())
            val httpCookie = HttpCookie(it.key, it.value.toString())
            httpCookie.path = "/"
            httpCookie.version = 0
            httpCookie.domain = "10.0.2.2:5000"
            cookieManager.cookieStore.add(URI("http://10.0.2.2:5000/"), httpCookie)
        }
        CookieHandler.setDefault(cookieManager)
    }
}

package pl.platform.tradingmobileapp.service.newposition

import java.io.Serializable
import java.math.BigDecimal
import java.sql.Timestamp

data class NewPositionRecord(
    val currencyPair: String,
    val bid: BigDecimal,
    val ask: BigDecimal,
    val timestamp: Timestamp
) : Serializable
package pl.platform.tradingmobileapp.service.pending

import java.io.Serializable
import java.math.BigDecimal
import java.sql.Timestamp

data class PendingOrderRecord(
    val oid: Long,
    val uid: Long,
    val tid: Long,
    val currencyPair: String,
    val amount: BigDecimal,
    val orderPrice: BigDecimal,
    val orderTimestamp: Timestamp,
    val longPosition: Boolean,
    val triggeredAbove: Boolean
) : Serializable

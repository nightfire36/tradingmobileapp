package pl.platform.tradingmobileapp.service.opened

import java.io.Serializable
import java.math.BigDecimal
import java.sql.Timestamp

data class OpenedPositionRecord(
    val tid: Long,
    val uid: Long,
    val currencyPair: String,
    val amount: BigDecimal,
    val openingPrice: BigDecimal,
    val openingTimestamp: Timestamp,
    val longPosition: Boolean,
    val currentProfit: BigDecimal
) : Serializable
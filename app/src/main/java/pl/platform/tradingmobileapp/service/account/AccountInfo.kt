package pl.platform.tradingmobileapp.service.account

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal
import java.sql.Timestamp

data class AccountInfo (
    val firstName: String,
    val lastName: String,
    val email: String,
    val createdAt: Timestamp,
    val accountBalance: BigDecimal
)

package pl.platform.tradingmobileapp.activity.platform.tab.newposition

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.service.newposition.NewPositionRecord

class NewPositionOnclickListener(
    private val newPositionRecyclerView: RecyclerView,
    private val adapter: NewPositionAdapter) : View.OnClickListener {
    override fun onClick(v: View?) {
        v?.let {
            val exchangeRate = adapter.getItem(newPositionRecyclerView.getChildAdapterPosition(it))

            val intent = Intent(it.context, NewPositionDetails::class.java).putExtra("exchange_rate", exchangeRate)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            it.context.startActivity(intent)
        }
    }
}

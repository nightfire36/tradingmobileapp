package pl.platform.tradingmobileapp.activity

interface LoginCheckCallback {
    fun onStatusSuccess()
    fun onUnauthorized()

}
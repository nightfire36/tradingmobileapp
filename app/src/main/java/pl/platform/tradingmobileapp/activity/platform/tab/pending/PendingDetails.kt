package pl.platform.tradingmobileapp.activity.platform.tab.pending

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.pending.PendingDetailsService
import pl.platform.tradingmobileapp.service.pending.PendingOrderRecord

class PendingDetails : AppCompatActivity() {

    lateinit var pendingOrder: PendingOrderRecord

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending_details)

        pendingOrder = intent.getSerializableExtra("pending_order") as PendingOrderRecord

        findViewById<TextView>(R.id.textView68).text = pendingOrder.oid.toString()
        findViewById<TextView>(R.id.textView69).text = pendingOrder.currencyPair
        findViewById<TextView>(R.id.textView70).text = pendingOrder.amount.toPlainString()
        findViewById<TextView>(R.id.textView71).text = pendingOrder.orderPrice.toPlainString()
        findViewById<TextView>(R.id.textView72).text = pendingOrder.orderTimestamp.toString()
        findViewById<TextView>(R.id.textView73).text = pendingOrder.longPosition.toString()
        findViewById<TextView>(R.id.textView74).text = pendingOrder.triggeredAbove.toString()
    }

    fun cancelOrderOnclick(view: View) {
        PendingDetailsService().cancelOrderRequest(pendingOrder.oid)
    }
}

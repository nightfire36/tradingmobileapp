package pl.platform.tradingmobileapp.activity.platform.tab.pending

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R

class PendingOrdersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val idTextView: TextView
    val pairTextView: TextView
    val amountTextView: TextView
    val priceTextView: TextView
    val positionTypeTextView: TextView

    init {
        idTextView = itemView.findViewById(R.id.textView27)
        pairTextView = itemView.findViewById(R.id.textView28)
        amountTextView = itemView.findViewById(R.id.textView29)
        priceTextView = itemView.findViewById(R.id.textView30)
        positionTypeTextView = itemView.findViewById(R.id.textView31)
    }
}

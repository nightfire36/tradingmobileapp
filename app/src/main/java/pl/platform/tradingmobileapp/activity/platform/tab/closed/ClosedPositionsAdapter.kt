package pl.platform.tradingmobileapp.activity.platform.tab.closed

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.closed.ClosedPositionRecord

class ClosedPositionsAdapter(
    private val closedPositions: List<ClosedPositionRecord>,
    private val closedRecyclerView: RecyclerView) :
        RecyclerView.Adapter<ClosedPositionsViewHolder>() {

    private val VIEW_TYPE_HEADER = 1
    private val VIEW_TYPE_ITEM = 2

    private val onClickListener = ClosedOnclickListener(closedRecyclerView, this)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClosedPositionsViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val closedPositionView = inflater.inflate(R.layout.item_closed_position_record, parent, false)

        closedPositionView.setOnClickListener(onClickListener)

        return ClosedPositionsViewHolder(closedPositionView)
    }

    override fun getItemCount(): Int {
        return closedPositions.size + 1
    }

    override fun onBindViewHolder(holder: ClosedPositionsViewHolder, position: Int) {
        if(isPositionHeader(position)) {
            holder.idTextView.text = "Id"
            holder.pairTextView.text = "Pair"
            holder.amountTextView.text = "Amount"
            holder.profitTextView.text = "Profit"
            holder.positionTypeTextView.text = "Position type"
        } else {
            val closedPosition = getItem(position)

            holder.idTextView.text = closedPosition.tid.toString()
            holder.pairTextView.text = closedPosition.currencyPair
            holder.amountTextView.text = closedPosition.amount.toPlainString()
            holder.profitTextView.text = closedPosition.profit.toPlainString()
            holder.positionTypeTextView.text = closedPosition.longPosition.toString()
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(isPositionHeader(position)) {
            return VIEW_TYPE_HEADER
        } else {
            return VIEW_TYPE_ITEM
        }
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(position: Int): ClosedPositionRecord {
        return closedPositions.get(position - 1)
    }
}

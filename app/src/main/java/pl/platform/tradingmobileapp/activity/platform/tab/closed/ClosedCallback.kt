package pl.platform.tradingmobileapp.activity.platform.tab.closed

import pl.platform.tradingmobileapp.service.closed.ClosedPositionRecord

interface ClosedCallback {

    fun onGetClosedPositionsSuccess(receivedClosedPositions: List<ClosedPositionRecord>)
    fun onGetClosedPositionsFail(status: Int)

}
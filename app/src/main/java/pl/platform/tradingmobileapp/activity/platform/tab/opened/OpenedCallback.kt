package pl.platform.tradingmobileapp.activity.platform.tab.opened

import pl.platform.tradingmobileapp.service.opened.OpenedPositionRecord

interface OpenedCallback {

    fun onGetOpenedPositionsSuccess(receivedOpenedPositions: List<OpenedPositionRecord>)
    fun onGetOpenedPositionsFail(status: Int)

}
package pl.platform.tradingmobileapp.activity.platform.tab.opened

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class OpenedOnclickListener(
    private val openedRecyclerView: RecyclerView,
    private val adapter: OpenedPositionsAdapter) : View.OnClickListener {
    override fun onClick(v: View?) {
        v?.let {
            val openedPosition = adapter.getItem(openedRecyclerView.getChildAdapterPosition(it))

            val intent = Intent(it.context, OpenedDetails::class.java).putExtra("opened_position", openedPosition)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            it.context.startActivity(intent)
        }
    }
}

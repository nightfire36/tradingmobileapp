package pl.platform.tradingmobileapp.activity.platform.tab.opened

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.closed.ClosedService
import pl.platform.tradingmobileapp.service.opened.OpenedDetailsService
import pl.platform.tradingmobileapp.service.opened.OpenedPositionRecord

class OpenedDetails : AppCompatActivity() {

    lateinit var openedPosition: OpenedPositionRecord

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opened_details)

        openedPosition = intent.getSerializableExtra("opened_position") as OpenedPositionRecord

        findViewById<TextView>(R.id.textView53).text = openedPosition.tid.toString()
        findViewById<TextView>(R.id.textView54).text = openedPosition.currencyPair
        findViewById<TextView>(R.id.textView55).text = openedPosition.amount.toPlainString()
        findViewById<TextView>(R.id.textView56).text = openedPosition.openingPrice.toPlainString()
        findViewById<TextView>(R.id.textView57).text = openedPosition.openingTimestamp.toString()
        findViewById<TextView>(R.id.textView58).text = openedPosition.longPosition.toString()
        findViewById<TextView>(R.id.textView59).text = openedPosition.currentProfit.toPlainString()
    }

    fun closePositionOnclick(view: View) {
        OpenedDetailsService().closePositionRequest(openedPosition.tid)
    }

    fun orderClosureOnclick(view: View) {
        OpenedDetailsService().orderClosureRequest(
            openedPosition.tid,
            findViewById<RadioButton>(R.id.radioButton4).isChecked,
            findViewById<EditText>(R.id.editText9)
                .text
                .toString()
                .toBigDecimal())
    }
}

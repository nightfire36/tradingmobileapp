package pl.platform.tradingmobileapp.activity.platform.tab.newposition

import pl.platform.tradingmobileapp.service.closed.ClosedPositionRecord
import pl.platform.tradingmobileapp.service.newposition.NewPositionRecord

interface NewPositionCallback {

    fun onGetRatesSuccess(receivedClosedPositions: List<NewPositionRecord>)
    fun onGetRatesFail(status: Int)

}
package pl.platform.tradingmobileapp.activity.platform.tab.pending

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.opened.OpenedPositionRecord
import pl.platform.tradingmobileapp.service.pending.PendingOrderRecord

class PendingOrdersAdapter(
    private val pendingOrders: List<PendingOrderRecord>,
    private val pendingRecyclerView: RecyclerView) :
        RecyclerView.Adapter<PendingOrdersViewHolder>() {

    private val VIEW_TYPE_HEADER = 1
    private val VIEW_TYPE_ITEM = 2

    private val onClickListener = PendingOnclickListener(pendingRecyclerView, this)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PendingOrdersViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val pendingOrderView = inflater.inflate(R.layout.item_pending_order_record, parent, false)

        pendingOrderView.setOnClickListener(onClickListener)

        return PendingOrdersViewHolder(pendingOrderView)
    }

    override fun getItemCount(): Int {
        return pendingOrders.size + 1
    }

    override fun onBindViewHolder(holder: PendingOrdersViewHolder, position: Int) {
        if(isPositionHeader(position)) {
            holder.idTextView.text = "Id"
            holder.pairTextView.text = "Pair"
            holder.amountTextView.text = "Amount"
            holder.priceTextView.text = "Price"
            holder.positionTypeTextView.text = "Position type"
        } else {
            val pendingOrder = getItem(position)

            holder.idTextView.text = pendingOrder.tid.toString()
            holder.pairTextView.text = pendingOrder.currencyPair
            holder.amountTextView.text = pendingOrder.amount.toPlainString()
            holder.priceTextView.text = pendingOrder.orderPrice.toPlainString()
            holder.positionTypeTextView.text = pendingOrder.longPosition.toString()
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(isPositionHeader(position)) {
            return VIEW_TYPE_HEADER
        } else {
            return VIEW_TYPE_ITEM
        }
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(position: Int): PendingOrderRecord {
        return pendingOrders.get(position - 1)
    }
}

package pl.platform.tradingmobileapp.activity.platform.tab.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.account.AccountInfo
import pl.platform.tradingmobileapp.service.account.AccountService
import java.text.SimpleDateFormat

class Account : Fragment(), AccountCallback {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AccountService(this).getAccountInfo()
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onGetAccountInfoSuccess(accountInfo: AccountInfo) {
        setTextViewById(R.id.textView8, accountInfo.firstName)
        setTextViewById(R.id.textView10, accountInfo.lastName)
        setTextViewById(R.id.textView12, accountInfo.email)
        setTextViewById(R.id.textView14, SimpleDateFormat().format(accountInfo.createdAt))
        setTextViewById(R.id.textView16, accountInfo.accountBalance.toPlainString())
    }

    private fun setTextViewById(textViewId: Int, textToSet: String) {
        activity?.findViewById<TextView>(textViewId)?.let { firstNameTextView ->
            firstNameTextView.text = textToSet
        }
    }

    override fun onGetAccountInfoFailed(status: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

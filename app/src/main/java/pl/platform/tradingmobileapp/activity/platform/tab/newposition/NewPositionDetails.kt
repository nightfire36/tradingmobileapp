package pl.platform.tradingmobileapp.activity.platform.tab.newposition

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.newposition.NewPositionDetailsService
import pl.platform.tradingmobileapp.service.newposition.NewPositionRecord

class NewPositionDetails : AppCompatActivity() {

    lateinit var exchangeRate: NewPositionRecord

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_position_details)

        exchangeRate = intent.getSerializableExtra("exchange_rate") as NewPositionRecord

        findViewById<TextView>(R.id.textView37).text = exchangeRate.currencyPair
        findViewById<TextView>(R.id.textView39).text = exchangeRate.ask.toPlainString()
        findViewById<TextView>(R.id.textView41).text = exchangeRate.bid.toPlainString()
        findViewById<TextView>(R.id.textView43).text = exchangeRate.timestamp.toString()

    }

    fun buyOnclick(view: View) {
        NewPositionDetailsService().buyRequest(exchangeRate.currencyPair,
            findViewById<EditText>(R.id.editText7)
                .text
                .toString()
                .toBigDecimal())
    }

    fun sellOnclick(view: View) {
        NewPositionDetailsService().sellRequest(exchangeRate.currencyPair,
            findViewById<EditText>(R.id.editText7)
                .text
                .toString()
                .toBigDecimal())
    }

    fun orderBuyOnclick(view: View) {
        NewPositionDetailsService().orderBuyRequest(exchangeRate.currencyPair,
            findViewById<EditText>(R.id.editText7)
                .text
                .toString()
                .toBigDecimal(),
            findViewById<RadioButton>(R.id.radioButton).isChecked,
            findViewById<EditText>(R.id.editText8)
                .text
                .toString()
                .toBigDecimal())
    }

    fun orderSellOnclick(view: View) {
        NewPositionDetailsService().orderSellRequest(exchangeRate.currencyPair,
            findViewById<EditText>(R.id.editText7)
                .text
                .toString()
                .toBigDecimal(),
            findViewById<RadioButton>(R.id.radioButton).isChecked,
            findViewById<EditText>(R.id.editText8)
                .text
                .toString()
                .toBigDecimal())
    }
}

package pl.platform.tradingmobileapp.activity.login

interface RegisterCallback {
    fun onRegisterSuccess()
    fun onRegisterFail(invalidFields: Int)
}
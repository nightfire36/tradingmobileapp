package pl.platform.tradingmobileapp.activity.platform.tab.pending

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.pending.PendingOrderRecord
import pl.platform.tradingmobileapp.service.pending.PendingService


class Pending : Fragment(), PendingCallback {

    private var pendingOrders  = ArrayList<PendingOrderRecord>()

    private lateinit var pendingOrdersRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_pending, container, false)

        pendingOrdersRecyclerView = rootView.findViewById<RecyclerView>(R.id.pendingOrdersList)

        pendingOrdersRecyclerView.adapter = PendingOrdersAdapter(pendingOrders, pendingOrdersRecyclerView)
        pendingOrdersRecyclerView.layoutManager = LinearLayoutManager(activity)

        PendingService(this).getPendingOrders()

        return rootView
    }

    override fun onGetPendingOrdersSuccess(receivedPendingOrders: List<PendingOrderRecord>) {
        pendingOrders.removeAll { true }
        pendingOrders.addAll(receivedPendingOrders)

        pendingOrdersRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onGetPendingOrdersFail(status: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

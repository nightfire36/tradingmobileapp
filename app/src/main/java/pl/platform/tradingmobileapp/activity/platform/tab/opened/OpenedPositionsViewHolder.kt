package pl.platform.tradingmobileapp.activity.platform.tab.opened

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R

class OpenedPositionsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val idTextView: TextView
    val pairTextView: TextView
    val amountTextView: TextView
    val profitTextView: TextView
    val positionTypeTextView: TextView

    init {
        idTextView = itemView.findViewById(R.id.textView22)
        pairTextView = itemView.findViewById(R.id.textView23)
        amountTextView = itemView.findViewById(R.id.textView24)
        profitTextView = itemView.findViewById(R.id.textView25)
        positionTypeTextView = itemView.findViewById(R.id.textView26)
    }
}

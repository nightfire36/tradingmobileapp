package pl.platform.tradingmobileapp.activity.platform.tab.closed

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.closed.ClosedPositionRecord

class ClosedDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_closed_details)

        val closedPosition: ClosedPositionRecord = intent.getSerializableExtra("closed_position") as ClosedPositionRecord

        findViewById<TextView>(R.id.textView84).text = closedPosition.tid.toString()
        findViewById<TextView>(R.id.textView85).text = closedPosition.currencyPair
        findViewById<TextView>(R.id.textView86).text = closedPosition.amount.toPlainString()
        findViewById<TextView>(R.id.textView87).text = closedPosition.openingPrice.toPlainString()
        findViewById<TextView>(R.id.textView88).text = closedPosition.closingPrice.toPlainString()
        findViewById<TextView>(R.id.textView89).text = closedPosition.openingTimestamp.toString()
        findViewById<TextView>(R.id.textView90).text = closedPosition.closingTimestamp.toString()
        findViewById<TextView>(R.id.textView91).text = closedPosition.profit.toPlainString()
        findViewById<TextView>(R.id.textView92).text = closedPosition.longPosition.toString()
    }
}

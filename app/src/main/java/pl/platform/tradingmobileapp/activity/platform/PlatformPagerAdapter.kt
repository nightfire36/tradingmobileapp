package pl.platform.tradingmobileapp.activity.platform

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import pl.platform.tradingmobileapp.activity.platform.tab.account.Account
import pl.platform.tradingmobileapp.activity.platform.tab.closed.Closed
import pl.platform.tradingmobileapp.activity.platform.tab.newposition.NewPosition
import pl.platform.tradingmobileapp.activity.platform.tab.opened.Opened
import pl.platform.tradingmobileapp.activity.platform.tab.pending.Pending

class PlatformPagerAdapter(context: Context, fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {
    val PAGE_COUNT = 5
    val pagesTitles = arrayOf("Account", "Closed", "Opened", "Pending", "New")

    override fun getItem(position: Int): Fragment {
        when(position) {
            0 -> return Account()
            1 -> return Closed()
            2 -> return Opened()
            3 -> return Pending()
            4 -> return NewPosition()
            else -> return Account()
        }
    }

    override fun getCount(): Int {
        return PAGE_COUNT
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return pagesTitles[position]
    }
}

package pl.platform.tradingmobileapp.activity.platform.tab.newposition

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R

class NewPositionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val pairTextView: TextView
    val askTextView: TextView
    val bidTextView: TextView
    val timestampTextView: TextView

    init {
        pairTextView = itemView.findViewById(R.id.textView32)
        askTextView = itemView.findViewById(R.id.textView33)
        bidTextView = itemView.findViewById(R.id.textView34)
        timestampTextView = itemView.findViewById(R.id.textView35)
    }
}

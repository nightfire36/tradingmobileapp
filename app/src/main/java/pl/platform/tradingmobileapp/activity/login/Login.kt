package pl.platform.tradingmobileapp.activity.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.activity.platform.Platform
import pl.platform.tradingmobileapp.service.login.LoginService
import java.net.CookieHandler
import java.net.CookieManager

class Login : AppCompatActivity(), LoginCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun submitOnclick(view: View) {
        val emailEditText = findViewById<EditText>(R.id.editText)
        val passwordEditText = findViewById<EditText>(R.id.editText2)
        if(!LoginService(emailEditText.text.toString(),
                passwordEditText.text.toString(), this).login()
        ) {
            Log.d("1", "Sending login request failed")
        }
    }

    private fun setAuthCookie() {
        val cookieManager = CookieHandler.getDefault() as CookieManager
        val cookiesList = cookieManager.cookieStore.cookies
        val sharedPreferences = getSharedPreferences("trading_mobile_app", Context.MODE_PRIVATE)
        cookiesList.forEach {
            Log.d("5", "cookie to save: " + it.toString())
            Log.d("5", it.value)
            sharedPreferences.edit().putString(it.name, it.value).commit()
        }
    }

    override fun onLoginSuccess() {
        setAuthCookie()
        val intent = Intent(this, Platform::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun onLoginFail(status: Int) {
        Log.d("1", "Login failed")
    }

    fun createAccountOnclick(view: View) {
        val intent = Intent(this, Register::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}

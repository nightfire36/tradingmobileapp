package pl.platform.tradingmobileapp.activity.platform.tab.pending

import pl.platform.tradingmobileapp.service.pending.PendingOrderRecord

interface PendingCallback {

    fun onGetPendingOrdersSuccess(receivedPendingOrders: List<PendingOrderRecord>)
    fun onGetPendingOrdersFail(status: Int)

}
package pl.platform.tradingmobileapp.activity.platform.tab.opened

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.opened.OpenedPositionRecord
import pl.platform.tradingmobileapp.service.opened.OpenedService

class Opened : Fragment(), OpenedCallback {

    private var openedPositions = ArrayList<OpenedPositionRecord>()

    private lateinit var openedPositionsRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_opened, container, false)

        openedPositionsRecyclerView = rootView.findViewById<RecyclerView>(R.id.openedPositionsList)

        openedPositionsRecyclerView.adapter = OpenedPositionsAdapter(openedPositions, openedPositionsRecyclerView)
        openedPositionsRecyclerView.layoutManager = LinearLayoutManager(activity)

        OpenedService(this).getOpenedPositions()

        return rootView
    }

    override fun onGetOpenedPositionsSuccess(receivedOpenedPositions: List<OpenedPositionRecord>) {
        openedPositions.removeAll { true }
        openedPositions.addAll(receivedOpenedPositions)

        openedPositionsRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onGetOpenedPositionsFail(status: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

package pl.platform.tradingmobileapp.activity.platform

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import pl.platform.tradingmobileapp.R

class Platform : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_platform)

        val viewPager = findViewById<ViewPager>(R.id.viewPager)
        val platformPagerAdapter =
            PlatformPagerAdapter(this, supportFragmentManager)

        viewPager.adapter = platformPagerAdapter

        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        tabLayout.setupWithViewPager(viewPager)
    }
}

package pl.platform.tradingmobileapp.activity.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.login.RegisterService

class Register : AppCompatActivity(), RegisterCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    fun registerOnclick(view: View) {
        val firstNameEditText = findViewById<EditText>(R.id.editText3)
        val lastNameEditText = findViewById<EditText>(R.id.editText4)
        val emailEditText = findViewById<EditText>(R.id.editText5)
        val passwordEditText = findViewById<EditText>(R.id.editText6)
        if(!RegisterService(firstNameEditText.getText().toString(),
                lastNameEditText.getText().toString(), emailEditText.getText().toString(),
                passwordEditText.getText().toString(), this).register()) {
            // request failed
        }
    }
    fun goToLogin() {
        val intent = Intent(this, Login::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    fun goToLoginOnclick(view: View) {
        goToLogin()
    }

    override fun onRegisterSuccess() {
        goToLogin()
    }

    override fun onRegisterFail(invalidFields: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

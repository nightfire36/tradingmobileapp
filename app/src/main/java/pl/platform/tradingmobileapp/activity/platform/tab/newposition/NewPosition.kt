package pl.platform.tradingmobileapp.activity.platform.tab.newposition

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.newposition.NewPositionRecord
import pl.platform.tradingmobileapp.service.newposition.NewPositionService

class NewPosition : Fragment(), NewPositionCallback {

    private var exchangeRates = ArrayList<NewPositionRecord>()

    private lateinit var newPositionRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_new, container, false)

        newPositionRecyclerView = rootView.findViewById<RecyclerView>(R.id.newPositionList)

        newPositionRecyclerView.adapter = NewPositionAdapter(exchangeRates, newPositionRecyclerView)
        newPositionRecyclerView.layoutManager = LinearLayoutManager(activity)

        NewPositionService(this).getRates()

        return rootView
    }

    override fun onGetRatesSuccess(receivedClosedPositions: List<NewPositionRecord>) {
        exchangeRates.removeAll { true }
        exchangeRates.addAll(receivedClosedPositions)

        newPositionRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onGetRatesFail(status: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

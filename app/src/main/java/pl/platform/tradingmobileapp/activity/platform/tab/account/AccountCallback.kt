package pl.platform.tradingmobileapp.activity.platform.tab.account

import pl.platform.tradingmobileapp.service.account.AccountInfo

interface AccountCallback {
    fun onGetAccountInfoSuccess(accountInfo: AccountInfo)
    fun onGetAccountInfoFailed(status: Int)
}

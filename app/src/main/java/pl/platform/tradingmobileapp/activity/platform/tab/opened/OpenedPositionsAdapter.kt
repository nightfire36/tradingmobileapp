package pl.platform.tradingmobileapp.activity.platform.tab.opened

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.opened.OpenedPositionRecord

class OpenedPositionsAdapter(
    private val openedPositions: List<OpenedPositionRecord>,
    private val openedRecyclerView: RecyclerView) :
        RecyclerView.Adapter<OpenedPositionsViewHolder>() {

    private val VIEW_TYPE_HEADER = 1
    private val VIEW_TYPE_ITEM = 2

    private val onClickListener = OpenedOnclickListener(openedRecyclerView, this)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OpenedPositionsViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val openedPositionView = inflater.inflate(R.layout.item_opened_position_record, parent, false)

        openedPositionView.setOnClickListener(onClickListener)

        return OpenedPositionsViewHolder(openedPositionView)
    }

    override fun getItemCount(): Int {
        return openedPositions.size + 1
    }

    override fun onBindViewHolder(holder: OpenedPositionsViewHolder, position: Int) {
        if(isPositionHeader(position)) {
            holder.idTextView.text = "Id"
            holder.pairTextView.text = "Pair"
            holder.amountTextView.text = "Amount"
            holder.profitTextView.text = "Profit"
            holder.positionTypeTextView.text = "Position type"
        } else {
            val openedPosition = getItem(position)

            holder.idTextView.text = openedPosition.tid.toString()
            holder.pairTextView.text = openedPosition.currencyPair
            holder.amountTextView.text = openedPosition.amount.toPlainString()
            holder.profitTextView.text = openedPosition.currentProfit.toPlainString()
            holder.positionTypeTextView.text = openedPosition.longPosition.toString()
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(isPositionHeader(position)) {
            return VIEW_TYPE_HEADER
        } else {
            return VIEW_TYPE_ITEM
        }
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(position: Int): OpenedPositionRecord {
        return openedPositions.get(position - 1)
    }
}

package pl.platform.tradingmobileapp.activity.platform.tab.closed

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R

class ClosedPositionsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val idTextView: TextView
    val pairTextView: TextView
    val amountTextView: TextView
    val profitTextView: TextView
    val positionTypeTextView: TextView

    init {
        idTextView = itemView.findViewById(R.id.textView17)
        pairTextView = itemView.findViewById(R.id.textView18)
        amountTextView = itemView.findViewById(R.id.textView19)
        profitTextView = itemView.findViewById(R.id.textView20)
        positionTypeTextView = itemView.findViewById(R.id.textView21)
    }
}

package pl.platform.tradingmobileapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import pl.platform.tradingmobileapp.activity.login.Login
import pl.platform.tradingmobileapp.activity.platform.Platform
import pl.platform.tradingmobileapp.service.InitializeCookieManager
import pl.platform.tradingmobileapp.service.login.LoginCheckService

class MainActivity : AppCompatActivity(),
    LoginCheckCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        InitializeCookieManager(this).initialize()
        loginCheck()
    }

    private fun loginCheck() {
        LoginCheckService(this).loginCheck()
    }

    private fun platformActivity() {
        val intent = Intent(this, Platform::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun loginActivity() {
        val intent = Intent(this, Login::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun onStatusSuccess() {
        platformActivity()
    }

    override fun onUnauthorized() {
        loginActivity()
    }
}

package pl.platform.tradingmobileapp.activity.platform.tab.newposition

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.closed.ClosedPositionRecord
import pl.platform.tradingmobileapp.service.newposition.NewPositionRecord

class NewPositionAdapter(
    private val exchangeRates: List<NewPositionRecord>,
    private val newPositionRecyclerView: RecyclerView) :
        RecyclerView.Adapter<NewPositionViewHolder>() {

    private val VIEW_TYPE_HEADER = 1
    private val VIEW_TYPE_ITEM = 2

    private val onClickListener = NewPositionOnclickListener(newPositionRecyclerView, this)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewPositionViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val newPositionView = inflater.inflate(R.layout.item_new_position_record, parent, false)

        newPositionView.setOnClickListener(onClickListener)

        return NewPositionViewHolder(newPositionView)
    }

    override fun getItemCount(): Int {
        return exchangeRates.size + 1
    }

    override fun onBindViewHolder(holder: NewPositionViewHolder, position: Int) {
        if(isPositionHeader(position)) {
            holder.pairTextView.text = "Pair"
            holder.askTextView.text = "Ask"
            holder.bidTextView.text = "Bid"
            holder.timestampTextView.text = "Timestamp"
        } else {
            val exchangeRate = getItem(position)

            holder.pairTextView.text = exchangeRate.currencyPair
            holder.askTextView.text = exchangeRate.ask.toPlainString()
            holder.bidTextView.text = exchangeRate.bid.toPlainString()
            holder.timestampTextView.text = exchangeRate.timestamp.toString()
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(isPositionHeader(position)) {
            return VIEW_TYPE_HEADER
        } else {
            return VIEW_TYPE_ITEM
        }
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(position: Int): NewPositionRecord {
        return exchangeRates.get(position - 1)
    }
}

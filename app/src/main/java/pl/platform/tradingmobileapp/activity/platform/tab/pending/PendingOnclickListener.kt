package pl.platform.tradingmobileapp.activity.platform.tab.pending

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class PendingOnclickListener(
    private val pendingRecyclerView: RecyclerView,
    private val adapter: PendingOrdersAdapter) : View.OnClickListener {
    override fun onClick(v: View?) {
        v?.let {
            val pendingOrder = adapter.getItem(pendingRecyclerView.getChildAdapterPosition(it))

            val intent = Intent(it.context, PendingDetails::class.java).putExtra("pending_order", pendingOrder)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            it.context.startActivity(intent)
        }
    }
}

package pl.platform.tradingmobileapp.activity.platform.tab.closed

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ClosedOnclickListener(
    private val closedRecyclerView: RecyclerView,
    private val adapter: ClosedPositionsAdapter) : View.OnClickListener {
    override fun onClick(v: View?) {
        v?.let {
            val closedPosition = adapter.getItem(closedRecyclerView.getChildAdapterPosition(it))

            val intent = Intent(it.context, ClosedDetails::class.java).putExtra("closed_position", closedPosition)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            it.context.startActivity(intent)
        }
    }
}

package pl.platform.tradingmobileapp.activity.platform.tab.closed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.platform.tradingmobileapp.R
import pl.platform.tradingmobileapp.service.closed.ClosedPositionRecord
import pl.platform.tradingmobileapp.service.closed.ClosedService
import java.math.BigDecimal
import java.util.*


class Closed : Fragment(), ClosedCallback {
    private var closedPositions = ArrayList<ClosedPositionRecord>()

    private lateinit var closedPositionsRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_closed, container, false)

        closedPositionsRecyclerView = rootView.findViewById<RecyclerView>(R.id.closedPositionsList)

        closedPositionsRecyclerView.adapter = ClosedPositionsAdapter(closedPositions, closedPositionsRecyclerView)
        closedPositionsRecyclerView.layoutManager = LinearLayoutManager(activity)

        ClosedService(this).getClosedPositions()

        return rootView
    }

    override fun onGetClosedPositionsSuccess(receivedClosedPositions: List<ClosedPositionRecord>) {
        closedPositions.removeAll { true }
        closedPositions.addAll(receivedClosedPositions)

        closedPositionsRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onGetClosedPositionsFail(status: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

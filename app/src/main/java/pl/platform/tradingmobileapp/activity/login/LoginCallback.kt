package pl.platform.tradingmobileapp.activity.login

interface LoginCallback {
    fun onLoginSuccess()
    fun onLoginFail(status: Int)
}